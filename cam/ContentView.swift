//
//  ContentView.swift
//  cam
//
//  Created by Ahmed Chouat on 09/03/2020.
//  Copyright © 2020 Ahmed Chouat. All rights reserved.


import SwiftUI
import AVFoundation

struct Pattern: Identifiable{
    let id = UUID()
    let image: UIImage
    let number: Int
}

struct CustomCameraPhotoView: View {
    
    @EnvironmentObject var imageCModel : ImageClassificationModel
    @State private var isPresented = false
    @State private var takePhoto = false
    fileprivate func classification(){
        self.imageCModel.updateClassification()
    }
    @State private var image: Image?
    @State private var showingCustomCamera = false
    @State private var inputImage: UIImage?
    
    var body: some View {
        
        NavigationView {
            VStack {
                self.imageCModel.image == nil
                    ?
                        PlaceHolderView()
                            .toAnyView()
                    :
                    ZStack{
                        Image(uiImage: self.imageCModel.image!)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .edgesIgnoringSafeArea(.bottom)
                            .navigationBarTitle("ML Classification", displayMode: .inline)
                            .onTapGesture {
                                self.classification()
                        } .edgesIgnoringSafeArea(.all)
                        Text(self.imageCModel.classificationText.localizedCapitalized)
                            .foregroundColor(.white)
                            .font(.system(size: 22))
                            .shadow(color: .black, radius: 1, x: 2, y: 3)
                            .padding()
                            .background(Rectangle().foregroundColor(Color.orange).opacity(0.33).cornerRadius(10))
                    }.toAnyView()
                //                ZStack {
                //                    Rectangle().fill(Color.secondary)
                //
                //                    if image != nil
                //                    {
                //                        image?
                //                            .resizable()
                //                            .aspectRatio(contentMode: .fill)
                //                    }
                //                }
                //                .onTapGesture {
                //                    self.showingCustomCamera = true
                //                }
                //            }
                //            .sheet(isPresented: $showingCustomCamera, onDismiss: loadImage) {
                //                CustomCameraView(image: self.$inputImage)
                //            }
                //            .edgesIgnoringSafeArea(.all)
                
            }.navigationBarItems(leading: Button(action: {
                self.takePhoto = false
                self.imageCModel.classificationText = "Tap to classification"
                self.isPresented.toggle()
            }, label: {Image(systemName: "photo")}).sheet(isPresented: $isPresented){
                showImagePicker(image: self.$imageCModel.image, takePhoto: self.$takePhoto)
                },
                                 trailing: Button(action: {
                                    self.showingCustomCamera = true
                                    self.imageCModel.classificationText = "Tap to classification"
                                    self.isPresented.toggle()
                                 }, label: {Image(systemName: "camera")})).font(.title).sheet(isPresented: $showingCustomCamera){
                                    CustomCameraView(image: self.$inputImage)}
            
        }
        
//        func loadImage() {
//            guard let inputImage = inputImage else { return }
//            image = Image(uiImage: inputImage)
//        }
    
    }
    
    struct CustomCameraView: View {

        @Binding var image: UIImage?
        @State var didTapCapture: Bool = false
        var body: some View {
            ZStack(alignment: .bottom) {

                CustomCameraRepresentable(image: self.$image, didTapCapture: $didTapCapture)
                CaptureButtonView().onTapGesture {
                    self.didTapCapture = true
                }
            }
        }

    }
    
    
    struct CustomCameraRepresentable: UIViewControllerRepresentable {

        @Environment(\.presentationMode) var presentationMode
        @Binding var image: UIImage?
        @Binding var didTapCapture: Bool

        func makeUIViewController(context: Context) -> CustomCameraController {
            let controller = CustomCameraController()
            controller.delegate = context.coordinator
            return controller
        }

        func updateUIViewController(_ cameraViewController: CustomCameraController, context: Context) {

            if(self.didTapCapture) {
                cameraViewController.didTapRecord()
            }
        }
        func makeCoordinator() -> Coordinator {
            Coordinator(self)
        }

        class Coordinator: NSObject, UINavigationControllerDelegate, AVCapturePhotoCaptureDelegate {
            let parent: CustomCameraRepresentable

            init(_ parent: CustomCameraRepresentable) {
                self.parent = parent
            }

            func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {

                parent.didTapCapture = false

                if let imageData = photo.fileDataRepresentation() {
                    parent.image = UIImage(data: imageData)
                }
                parent.presentationMode.wrappedValue.dismiss()
            }
        }
    }
    
    class CustomCameraController: UIViewController {

        var image: UIImage?

        var captureSession = AVCaptureSession()
        var backCamera: AVCaptureDevice?
        var frontCamera: AVCaptureDevice?
        var currentCamera: AVCaptureDevice?
        var photoOutput: AVCapturePhotoOutput?
        var cameraPreviewLayer: AVCaptureVideoPreviewLayer?

        //DELEGATE
        var delegate: AVCapturePhotoCaptureDelegate?

        func didTapRecord() {

            let settings = AVCapturePhotoSettings()
            photoOutput?.capturePhoto(with: settings, delegate: delegate!)

        }

        override func viewDidLoad() {
            super.viewDidLoad()
            setup()
        }
        func setup() {
            setupCaptureSession()
            setupDevice()
            setupInputOutput()
            setupPreviewLayer()
            startRunningCaptureSession()
        }
        func setupCaptureSession() {
            captureSession.sessionPreset = AVCaptureSession.Preset.photo
        }

        func setupDevice() {
            let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera],
                                                                          mediaType: AVMediaType.video,
                                                                          position: AVCaptureDevice.Position.unspecified)
            for device in deviceDiscoverySession.devices {

                switch device.position {
                case AVCaptureDevice.Position.front:
                    self.frontCamera = device
                case AVCaptureDevice.Position.back:
                    self.backCamera = device
                default:
                    break
                }
            }

            self.currentCamera = self.backCamera
        }


        func setupInputOutput() {
            do {

                let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
                captureSession.addInput(captureDeviceInput)
                photoOutput = AVCapturePhotoOutput()
                photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
                captureSession.addOutput(photoOutput!)

            } catch {
                print(error)
            }

        }
        func setupPreviewLayer()
        {
            self.cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            self.cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            self.cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
            self.cameraPreviewLayer?.frame = self.view.frame
            self.view.layer.insertSublayer(cameraPreviewLayer!, at: 0)

        }
        func startRunningCaptureSession(){
            captureSession.startRunning()
        }
    }
    
    
    struct CaptureButtonView: View {
        @State private var showingPatterns = false
        @State private var chosenPattern = 0
        @State private var bgArray1 = ["0c", "1c", "2c", "3c", "4c"]
        @State private var bgArray2 = ["0cb", "1cb", "2cb", "3cb", "4cb"]
        @State private var bgArray3 = ["0p", "1p", "2p", "3p", "4p"]
        @State private var animationAmount: CGFloat = 1
       
        @State private var Patterns = [
            Pattern(image: #imageLiteral(resourceName: "0.png"), number: 1),
        Pattern(image: #imageLiteral(resourceName: "1.png"), number: 2),
        Pattern(image: #imageLiteral(resourceName: "2.png"), number: 3),
        Pattern(image: #imageLiteral(resourceName: "3.png"), number: 4),
        Pattern(image: #imageLiteral(resourceName: "4.png"), number: 5)]
         var capturePhotoOutput: AVCapturePhotoOutput?
        var body: some View {
            
            VStack {
                
                Spacer()
                
               
                
                if showingPatterns {
                    Image(bgArray2[chosenPattern])
                        .overlay(
                            Circle()
                                .stroke(Color.red)
                                .scaleEffect(self.animationAmount)
                                .opacity(Double(2 - self.animationAmount))
                                .animation(Animation.easeOut(duration: 1)
                                    .repeatForever(autoreverses: true))
                        )
                        .onAppear
                        {
                            self.animationAmount = 2
                }
                    //                Image(bgArray3[chosenPattern])
                    //                    .opacity(0.3)
                } else {
                    ScrollView(.horizontal) {
                        HStack {
                            ForEach(0 ..< 5, id: \.self) { number in
                                Button(action: {
                                }
                                ) {
                                    if number == self.chosenPattern {
                                        Image(self.bgArray1[number])
                                        
                                    } else {
                                        Image(self.bgArray1[number])
                                    }
                                    
                                }
                            }
                        }.buttonStyle(PlainButtonStyle())
                    }
                    
                
                
            }
                //        }
                
            
        }.gesture(
        TapGesture()
            .onEnded({ _ in
                self.showingPatterns.toggle()
            }))
    }
        
    }
    
    struct PlaceHolderView: View{
        var body: some View{
            ZStack{
                Color(.systemBackground)
                Image(systemName: "umbrella")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(Color.init(.systemRed))
                    .shadow(color: .secondary, radius: 5)
            }.padding()
        }
    }
    
}
    struct ContentView_Previews: PreviewProvider {
        static var previews: some View {
            CustomCameraPhotoView()
                .environmentObject(ImageClassificationModel())
        }
}
