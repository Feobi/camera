//
//  View + extension.swift
//  cam
//
//  Created by Angelo Minieri on 10/03/2020.
//  Copyright © 2020 Ahmed Chouat. All rights reserved.
//

import SwiftUI
extension View{
    func toAnyView() -> AnyView{AnyView(self)}
}
