//
//  UIImage + extension.swift
//  cam
//
//  Created by Angelo Minieri on 10/03/2020.
//  Copyright © 2020 Ahmed Chouat. All rights reserved.
//

import Foundation
import UIKit
import ImageIO

extension CGImagePropertyOrientation{
    init(_ orientation: UIImage.Orientation) {
        switch orientation {
        case .upMirrored: self = .upMirrored
        case .down: self = .down
        case .downMirrored: self = .downMirrored
        case .left: self = .left
        case .leftMirrored: self = .leftMirrored
        case .right: self = .right
        case .rightMirrored: self = .rightMirrored
        default: self = .up
        }
    }
}

extension CGImagePropertyOrientation{
    init(_ orientation: UIDeviceOrientation) {
        switch orientation {
        case .portraitUpsideDown: self = .left
        case .landscapeLeft: self = .up
        case .landscapeRight: self = .down
        default:
            self = .right
        }
    }
}

