//
//  ImageClassificationModel.swift
//  cam
//
//  Created by Angelo Minieri on 10/03/2020.
//  Copyright © 2020 Ahmed Chouat. All rights reserved.
//

import UIKit
import CoreML
import Vision
import ImageIO


final class ImageClassificationModel: ObservableObject{
    @Published var image: UIImage? = nil
    @Published var classificationText: String = ""
    
    private lazy var classificationRequest: VNCoreMLRequest = {
        do{
            let model = try VNCoreMLModel(for: camera2().model)
            let request = VNCoreMLRequest(model: model, completionHandler : {
                [weak self] request, error in
                self?.processClassification(for: request, error: error)
            })
            request.imageCropAndScaleOption = .centerCrop
            return request
        }catch {
            fatalError("Failed to load Vision ML Model \(error)")
        }
    }()
    
    func updateClassification(){
        self.classificationText = "Classifying..."
        let orientation = CGImagePropertyOrientation(self.image!.imageOrientation)
        guard let ciImage = CIImage(image: self.image!) else {
            fatalError("Unable to create \(CIImage.self) from \(String(describing: image))!")}
        DispatchQueue.global(qos: .userInitiated).async {
            let handler = VNImageRequestHandler(ciImage: ciImage, orientation: orientation)
            do{
                try handler.perform([self.classificationRequest])
            }catch{
                print("Failed to perform classification. \n\(error.localizedDescription)")
            }
        }
        }
    
    private func processClassification(for request: VNRequest, error: Error?){
        DispatchQueue.main.async {
            if let results = request.results as? [VNClassificationObservation]{
                if results.isEmpty{
                    self.classificationText = "nothing found"
                } else if results[0].confidence < 0.85{
                    self.classificationText = "Not sure"
                }else {
                    self.classificationText = String(format: "%@%.1f%%", results[0].identifier, results[0].confidence * 100)
                }
            }
            else if let error = error{
                self.classificationText = "error: \(error.localizedDescription)"
            }else {
                self.classificationText = "???"
            }
            
        }
        
    }
}
